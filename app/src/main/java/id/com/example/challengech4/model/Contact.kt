package id.com.example.challengech4.model

data class Contact(
    val nama: String,
    val phone: String
)
